import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/inputr0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_password'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_password'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_Home/li_groupsSwitch Team'))

WebUI.setText(findTestObject('Object Repository/Page_Home/inputr8'), '36dce712-a9eb-4275-96d8-dcaf74c17cc5')

WebUI.sendKeys(findTestObject('Object Repository/Page_Home/inputr8'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/button_ChatDaddy Bug TestingChatDaddy Bug Testing'))

WebUI.click(findTestObject('Object Repository/Page_Home/div_MuiGrid-root css-1d2hfx'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Products'))

WebUI.click(findTestObject('Object Repository/Page_Products/button_addCreate'))

WebUI.setText(findTestObject('Object Repository/Page_Products - New/inputr1k'), 'test')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/textarea_t'), 't')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/textarea_te'), 'te')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/textarea_tes'), 'tes')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/textarea_test'), 'test')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/inputr1m'), '10')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/inputr1n'), '')

WebUI.click(findTestObject('Object Repository/Page_Products - New/inputr1n'))

WebUI.setText(findTestObject('Object Repository/Page_Products - New/textarea_test_1'), 'test\n')

WebUI.setText(findTestObject('Object Repository/Page_Products - New/inputr1m'), '10')

WebUI.click(findTestObject('Object Repository/Page_Products - New/inputr1m'))

WebUI.setText(findTestObject('Object Repository/Page_Products - New/inputr1n'), '1000')

WebUI.sendKeys(findTestObject('Object Repository/Page_Products - New/inputr1n'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Products - New/inputshopProduct'))

WebUI.click(findTestObject('Object Repository/Page_Products - New/p_Click to Uploador drag and drop, JPG, PNG or SVG'))

WebUI.click(findTestObject('Object Repository/Page_Products - New/button_Create'))

WebUI.click(findTestObject('Object Repository/Page_Products/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_Products/span_Logout'))

WebUI.closeBrowser()

