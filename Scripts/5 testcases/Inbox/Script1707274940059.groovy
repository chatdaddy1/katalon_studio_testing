import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/inputr0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_password (7)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_password (7)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/span_account_circle (7)'))

WebUI.click(findTestObject('Object Repository/Page_Home/li_groupsSwitch Team (6)'))

WebUI.setText(findTestObject('Object Repository/Page_Home/inputr8 (5)'), '2f609d4f-4cd4-4c11-b021-ef841d1a1051')

WebUI.sendKeys(findTestObject('Object Repository/Page_Home/inputr8 (5)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/button_Ivans TeamIvan Testing (1)'))

WebUI.click(findTestObject('Object Repository/Page_Home/div_MuiGrid-root css-1d2hfx (6)'))

WebUI.click(findTestObject('Object Repository/Page_Home/a_Inbox'))

WebUI.click(findTestObject('Object Repository/Page_(3) Inbox/span_Skip this Short and Sweet Tour (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox/inputr5 (1)'), 'jo testing')

WebUI.sendKeys(findTestObject('Object Repository/Page_Inbox/inputr5 (1)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Inbox/div_Jo testing3m'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Jo testing/div_Jo testing3m (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/li_push_pinPin Chat (1)'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Jo testing/div_Jo testing3m (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/li_push_pinUnpin Chat (1)'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Jo testing/div_Jo testing3m (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/li_file_uploadExport Chat History (1)'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Jo testing/div_Jo testing3m (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/li_perm_mediaExport Media (last 48hrs) (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/button_ChatDaddy AI (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/input_PrivateSwitchBase-input css-1m9pwf3 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/button_Send (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/div_MuiGrid-root MuiGrid-container MuiGrid-_f4d2a9 (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox - Jo testing/textarea (1)'), 'test')

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/span_send (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/div_MuiGrid-root MuiGrid-container MuiGrid-_f4d2a9 (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox - Jo testing/textarea (1)'), 'hesllo')

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/span_send (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/span_search (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox - Jo testing/inputrl'), 'test')

WebUI.sendKeys(findTestObject('Object Repository/Page_Inbox - Jo testing/inputrl'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/p_February 7, 2024 at 1047 AM'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/span_expand_more (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/li_portraitContact Info (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Jo testing/span_close (1)'))

WebUI.closeBrowser()

