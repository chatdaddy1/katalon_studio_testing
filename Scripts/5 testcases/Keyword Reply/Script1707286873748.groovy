import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/inputr0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_password (12)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_password (12)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/span_account_circle (12)'))

WebUI.click(findTestObject('Object Repository/Page_Home/li_groupsSwitch Team (11)'))

WebUI.setText(findTestObject('Object Repository/Page_Home/inputr8 (9)'), '36dce712-a9eb-4275-96d8-dcaf74c17cc5')

WebUI.sendKeys(findTestObject('Object Repository/Page_Home/inputr8 (9)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/button_ChatDaddy Bug TestingChatDaddy Bug Testing (9)'))

WebUI.click(findTestObject('Object Repository/Page_Home/div_MuiGrid-root css-1d2hfx (11)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Keyword Reply (6)'))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply/span_more_vert (5)'))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply/li_editEdit (5)'))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply - Contains Phrase hello/inputr14 (2)'))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply - Contains Phrase hello/path (2)'))

WebUI.setText(findTestObject('Object Repository/Page_Keyword Reply - Contains Phrase hello/inputr14_1'), keyword)

WebUI.sendKeys(findTestObject('Object Repository/Page_Keyword Reply - Contains Phrase hello/inputr14_1'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply - Contains Phrase hello/div_Save (2)'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Keyword Reply/span_hello (4)'), expectedText)

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply/span_account_circle (5)'))

WebUI.click(findTestObject('Object Repository/Page_Keyword Reply/span_Logout (4)'))

WebUI.closeBrowser()

