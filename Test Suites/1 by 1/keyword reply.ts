<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>keyword reply</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a40962d3-fcce-4b84-ab1c-88bbb10a9082</testSuiteGuid>
   <testCaseLink>
      <guid>49e8935c-a743-4a80-93fe-6ac3a3a925df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 testcases/Keyword Reply</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0c25e76f-273f-4acf-86cd-0f5c0e1ae2d0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/keyword reply</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>0c25e76f-273f-4acf-86cd-0f5c0e1ae2d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>keyword</value>
         <variableId>16c98da6-8fb5-489b-8d68-7a16ef0b9252</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0c25e76f-273f-4acf-86cd-0f5c0e1ae2d0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expectedText</value>
         <variableId>a775ad32-d591-4fae-8c87-657f4b48bc0c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
