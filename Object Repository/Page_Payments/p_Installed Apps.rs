<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Installed Apps</name>
   <tag></tag>
   <elementGuidId>bb38a3b1-77d4-49f0-94ec-e50c0ab30c1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div[2]/div/div[2]/div/button[2]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.css-done1p > p.MuiTypography-root.MuiTypography-body1.css-1l0poxv</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>ca07257f-d954-4ac0-a45c-9c2353f0ff19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-1l0poxv</value>
      <webElementGuid>b41f7dad-a901-4b27-b604-8cab84354351</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Installed Apps</value>
      <webElementGuid>c3d65ea5-40ae-4893-ae69-5563007c9e36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-du3r8y&quot;]/div[@class=&quot;flex-column MuiBox-root css-8atqhb&quot;]/div[@class=&quot;MuiBox-root css-8atqhb&quot;]/div[@class=&quot;flex-between-center MuiBox-root css-acwcvw&quot;]/div[@class=&quot;flex-start-center MuiBox-root css-0&quot;]/button[@class=&quot;MuiButtonBase-root css-done1p&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-1l0poxv&quot;]</value>
      <webElementGuid>22b46336-5f70-4798-93ae-e54db6f2e721</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div[2]/div/div[2]/div/button[2]/p</value>
      <webElementGuid>331c8fb7-4c73-4287-bb1a-847bcb2983a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/p</value>
      <webElementGuid>2e1ba5f8-4de0-430a-bc78-f8f43b02f11b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Installed Apps' or . = 'Installed Apps')]</value>
      <webElementGuid>cebc6d3f-7862-41c6-abdd-5e87943c4192</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
