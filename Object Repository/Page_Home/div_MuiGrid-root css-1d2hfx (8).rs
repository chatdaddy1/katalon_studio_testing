<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_MuiGrid-root css-1d2hfx (8)</name>
   <tag></tag>
   <elementGuidId>902da012-44fc-49ea-8e34-2a48f45cfc1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-1d2hfx</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[2]/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>488b8688-12c1-4c4f-b4c7-ac2fc3215144</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1d2hfx</value>
      <webElementGuid>f973f820-0de0-4cfb-babb-3f63abb74431</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiBox-root css-1vp4vxd&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]</value>
      <webElementGuid>68b95528-d487-40bc-92cb-ba5e18290cb4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div[4]</value>
      <webElementGuid>86b1d62a-d3c8-4af1-9183-b0bf66893aad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]</value>
      <webElementGuid>60dea3aa-a77a-437e-a46a-1d35c47afc8b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
