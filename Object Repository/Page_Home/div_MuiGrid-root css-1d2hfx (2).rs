<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_MuiGrid-root css-1d2hfx (2)</name>
   <tag></tag>
   <elementGuidId>f74a024d-6b8c-4f06-b191-183b4c088a9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[2]/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-1d2hfx</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7be203e1-b4e1-4533-a6c1-b6a04edfb901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1d2hfx</value>
      <webElementGuid>52c78a14-94c1-4ded-b144-8d25ea40619a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiBox-root css-1vp4vxd&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]</value>
      <webElementGuid>1a205bda-6958-4b2c-bd5a-edb69a92f67e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div[3]</value>
      <webElementGuid>72c95ed1-004a-47f1-975d-994d653ac949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]</value>
      <webElementGuid>2dccad53-0e57-4fd1-8f6e-5be0ec30b637</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
