<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Keyword Reply (1)</name>
   <tag></tag>
   <elementGuidId>a142f521-96fb-482e-8f62-f822e01a5743</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped > span.MuiTypography-root.MuiTypography-baseMedium.css-57mpad</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[3]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ee28cda7-c615-4b7a-afeb-e43440cb5109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-57mpad</value>
      <webElementGuid>168fcb83-235b-43f2-bb28-710745d14847</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Keyword Reply</value>
      <webElementGuid>5561f9f4-7365-48ee-a2b2-b68e0784243b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopper-root css-1awb5sy&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-57mpad&quot;]</value>
      <webElementGuid>3679a5fa-b914-4eaa-8d0c-63f2da9a5935</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]/span</value>
      <webElementGuid>8e7470c1-c4b7-4fb8-af52-b1dc8ce156a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Keyword Reply' or . = 'Keyword Reply')]</value>
      <webElementGuid>ae208e94-1a98-40be-83c3-4fc0465eb6b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
