<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Inbox</name>
   <tag></tag>
   <elementGuidId>df8542ff-b930-4c33-8e0b-b4b1e7b0162f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(@href, '/root-inbox/inbox')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9d1b737b-76cf-4c89-9471-e3d97eb319f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1vo1ped</value>
      <webElementGuid>a1fd96aa-1897-4a06-9014-6b1f9c15d54b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/root-inbox/inbox</value>
      <webElementGuid>6fa1f7a4-64f0-4ef4-a3b1-716a17c5f6a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Inbox</value>
      <webElementGuid>4bf69d91-a247-442a-a838-314eb9304a0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopper-root css-1awb5sy&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]</value>
      <webElementGuid>6de922d5-e02a-47ee-addb-af946c7665c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/root-inbox/inbox')])[2]</value>
      <webElementGuid>14271199-0dfe-4055-b163-7f8e9c92766d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a</value>
      <webElementGuid>6dee5b8a-47e5-4d6e-8a1c-306f0392ec1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/root-inbox/inbox' and (text() = 'Inbox' or . = 'Inbox')]</value>
      <webElementGuid>942f17b7-ed05-469b-bee5-2398f9e4f38a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
