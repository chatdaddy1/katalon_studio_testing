<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_MuiGrid-root css-1d2hfx (11)</name>
   <tag></tag>
   <elementGuidId>e0ea4720-8dd6-4eee-9836-463307fcd2cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[2]/div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-1d2hfx</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d595391-c139-4fc0-bee2-45d60c56b53e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiGrid-root css-1d2hfx</value>
      <webElementGuid>835815bc-f42a-4254-89a8-17e09a01e411</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiBox-root css-1vp4vxd&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]</value>
      <webElementGuid>f7451dd9-7a36-4d0f-8146-133be8ac5e0e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div[4]</value>
      <webElementGuid>1293979d-32e7-4deb-82fd-bf74ba02b0c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]</value>
      <webElementGuid>a02fecc7-38e1-46be-a561-513d77c33d99</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
