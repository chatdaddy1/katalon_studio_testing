<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Keyword Reply (6)</name>
   <tag></tag>
   <elementGuidId>8d916874-9df5-4bb6-9a3c-1f117da46f40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[3]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.MuiGrid-root.css-1vo1ped > span.MuiTypography-root.MuiTypography-baseMedium.css-57mpad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>10924560-8af8-409e-a3ea-60394ff0daed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-57mpad</value>
      <webElementGuid>60cdefbd-678b-44a3-b4ea-6b36f5beec5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Keyword Reply</value>
      <webElementGuid>a68aa0bd-19f4-4f96-bdd1-b796c5c7b1fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopper-root css-1awb5sy&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-57mpad&quot;]</value>
      <webElementGuid>69d9c0c7-8268-4598-816d-3a740bb264b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]/span</value>
      <webElementGuid>8e071af2-f464-4dbe-9ecc-488e07d334c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Keyword Reply' or . = 'Keyword Reply')]</value>
      <webElementGuid>ea0ad5f2-5460-4395-b994-27e55c59e74c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
