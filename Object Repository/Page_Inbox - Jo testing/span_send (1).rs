<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_send (1)</name>
   <tag></tag>
   <elementGuidId>8386b95d-a484-41f3-b53b-ef6b4f38c048</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-outlined.MuiBox-root.css-16qv2i2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div[3]/div[2]/div/div[2]/div[2]/div[2]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b81ea74e-9121-41ab-a4d4-aec0e40cf70a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-16qv2i2</value>
      <webElementGuid>7531757f-715e-44ae-8df3-914fd9bc6210</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>send</value>
      <webElementGuid>3610f828-0d71-40fa-9fdf-ad9d40985ffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-1kfrwse&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-10jvq4d&quot;]/div[@class=&quot;tour-notes-compose MuiBox-root css-13plktm&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-item MuiGrid-wrap-xs-nowrap css-jha8m4&quot;]/div[@class=&quot;MuiGrid-root css-gfyrbd&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-1jwxc8t&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-16qv2i2&quot;]</value>
      <webElementGuid>eae068e9-1984-433f-8475-34043f317455</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div[3]/div[2]/div/div[2]/div[2]/div[2]/button/span</value>
      <webElementGuid>1adec9ef-85e4-40bc-a7ed-9b83432a71d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div[2]/div[2]/button/span</value>
      <webElementGuid>8b6288d9-c43d-4ef6-9785-a2a2ad8f5da2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'send' or . = 'send')]</value>
      <webElementGuid>cdea5c72-73d0-4a17-92e4-b2fe79349ef3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
