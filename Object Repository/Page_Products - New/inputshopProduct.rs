<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputshopProduct</name>
   <tag></tag>
   <elementGuidId>41d8e62f-8c35-4448-a54c-040538aca1e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='shopProduct']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#shopProduct</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>324f46ca-9d38-4bf6-8d37-fd5111dd90b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3</value>
      <webElementGuid>2ac5e24b-7f20-4e32-af96-a51ad5541278</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shopProduct</value>
      <webElementGuid>330b3281-4f9e-442d-b6e6-0a4f9be4f7f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>f0ecacce-8ee0-4f8f-8f55-734a877d1551</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shopProduct&quot;)</value>
      <webElementGuid>1f565a32-c063-4563-a6fb-fbe81ecd8ad7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='shopProduct']</value>
      <webElementGuid>d596a57c-4a47-4d96-bf10-d7a58d486e25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div[2]/div/div/div/div/div/div/div/div[8]/div[2]/span/span/input</value>
      <webElementGuid>03b7e5b7-e978-4c2a-8e7f-8e453b7a8535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span/input</value>
      <webElementGuid>89175fb9-6c12-4a4e-9d6b-bf330aaf5791</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'shopProduct' and @type = 'checkbox']</value>
      <webElementGuid>033d6e31-3ce8-416e-aa7a-8dabe3ca2426</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
