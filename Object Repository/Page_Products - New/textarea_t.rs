<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_t</name>
   <tag></tag>
   <elementGuidId>102593ac-00d0-4ed8-a706-75d542acbe21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id=':r1l:']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>d0ec171c-d60a-44dc-9c1c-5e3c3f4ddd67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ad4a0d4b-8e7d-4cf6-b2ee-6ebdfa509de7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r1l:</value>
      <webElementGuid>03411520-a10b-402b-a687-4cd04ace921e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Add a description for the product!</value>
      <webElementGuid>0c757a3b-2448-40de-b66a-0a0c2c118e28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputMultiline MuiInputBase-inputSizeSmall css-fjc6h5</value>
      <webElementGuid>e8c3c9cc-07e1-4f87-a431-60a700753945</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>t</value>
      <webElementGuid>b16ce58b-0696-4f75-8ea2-e87a831dc40b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r1l:&quot;)</value>
      <webElementGuid>f426a013-77cb-47cc-b763-708048eb8d6b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id=':r1l:']</value>
      <webElementGuid>1fd0e653-bb87-4bfc-920c-36e7e28b21b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div[2]/div/div/div/div/div/div/div/div[4]/div[2]/div/div/textarea</value>
      <webElementGuid>6c471404-2e68-4fa4-9a4c-ba525cb99987</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>70a3bac5-8a5f-4992-b289-66e52cf831ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = ':r1l:' and @placeholder = 'Add a description for the product!' and (text() = 't' or . = 't')]</value>
      <webElementGuid>a04b71bb-1333-4e34-a8c5-278540faba0b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
